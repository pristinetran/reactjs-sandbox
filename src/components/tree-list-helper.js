import _ from 'lodash'

const sortingData = data => _.sortBy(data, [
  (o) => o.value.match(/^\w+/i),
  (o) => o.value.split('.').length > 1 && o.value.split('.')[1],
  (o) => o.value.split('.').length,
  (o) => o.value.split('.').length > 2 && o.value.split('.')[2].toLowerCase()
])

function pushObj (o, arr, keys, titles, hierarchy = true, debug = false) {
  let obj = {
    key: o.value,
    title: o.label
  }

  let idx = 0

  if (hierarchy) {
    let lastIdx = o.value.lastIndexOf('.')
    obj.main = (lastIdx > -1) ? o.value.slice(0, lastIdx) : o.value
    obj.identifier = o.value.slice(lastIdx + 1)
    idx = _.findIndex(arr, { key: obj.main })
  }

  if (debug) console.log(idx, obj)

  if (arr.length === 0) {
    obj.children = []

    if (!hierarchy) { obj.main = 'parent' }

    arr.push(obj)
    keys.push(obj.key)
    titles.push(obj.title)
  } else if (idx > -1) {
    if (hierarchy) {
      arr[idx].children = arr[idx].children || []
    } else { obj.main = 'child' }

    arr[idx].children.push(obj)
    keys.push(obj.key)
    titles.push(obj.title)
  }
}

function buildDataList (data, type) {
  let arrayList = [], allKeys = [], allTitles = []

  let sortedData = sortingData(data)
  console.warn('sortedData', sortedData)

  if (type === 'hierarchy') {
    sortedData.forEach(function (obj) {
      let partials = obj.value.split('.')
      if (partials.length < 3) {
        pushObj(obj, arrayList, allKeys, allTitles)
      } else {
        let l1 = arrayList.length - 1
        if (partials.length === 3) {
          pushObj(obj, arrayList[l1].children, allKeys, allTitles)
        }

        let l2 = arrayList[l1].children.length - 1
        if (partials.length === 4) {
          pushObj(obj, arrayList[l1].children[l2].children, allKeys, allTitles)
        }
      }
    })
  } else {
    // flat type
    sortedData.forEach(obj => pushObj(obj, arrayList, allKeys, allTitles, false))
  }

  return {
    arrayList,
    allKeys,
    allTitles
  }
}

const rinse = (data, output, children) => {
  let partitions = _.partition(_.map(children, 'key'), k => _.includes(data, k))
  output = partitions[0]

  let lv1Diff = partitions[1]

  _.map(lv1Diff, d => {
    let lv2children = _.find(children, { key: d }).children
    let lv2Keys = _.map(lv2children, 'key')
    partitions = _.partition(lv2Keys, k => _.includes(data, k))

    // push intersect values to output
    output.push(...partitions[0])

    let lv2Diff = partitions[1]

    _.map(lv2Diff, d2 => {
      let lv3children = _.find(lv2children, { key: d2 }).children
      let lv3Keys = _.map(lv3children, 'key')
      partitions = _.partition(lv3Keys, k => _.includes(data, k))

      // push intersect values to output
      output.push(...partitions[0])
    })
  })

  return output
}

function sanitizeDataForAjax (data, source, type) {
  let output = []

  if (data && data.length > 0 && source && source.arrayList) {
    if (type === 'hierarchy') {
      if (_.includes(data, source.arrayList[0].key)) {
        // sending top parent only
        output = [source.arrayList[0].key]
      } else {
        // find next level parent
        output = rinse(data, output, source.arrayList[0].children)
      }
    } else {
      output = data.filter(d => d !== 'select-all')
    }
  } else {
    console.error('data integrity issue')
  }

  return output
}

function HashMapToArray (data) {
  return _.map(data, (value, key) => ({ key, value }))
}

export default {
  buildDataList,
  sanitizeDataForAjax,
  HashMapToArray
}
