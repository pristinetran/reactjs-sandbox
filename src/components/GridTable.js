import React, { Component } from 'react'

export default class GridTable extends Component {
  render () {
    return (
      <div id='info_table_wrap'>
        <div className='row info_table'>
          <div className='col-xs-3 info_table'>
            HEAD1
          </div>
          <div className='col-xs-3 info_table'>
            HEAD2
          </div>
          <div className='col-xs-3 info_table'>
            HEAD2
          </div>
          <div className='col-xs-3 info_table'>
            HEAD4
          </div>
        </div>

        <div className='row info_table'>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>FOO</div>
            <div className='tabellenzelle_r'>123</div>
          </div>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>TEST1</div>
            <div className='tabellenzelle_r'>ABC</div>
          </div>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>FOO</div>
            <div className='tabellenzelle_r'>123</div>
          </div>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>TEST1</div>
            <div className='tabellenzelle_r'>ABC</div>
          </div>
        </div>

        <div className='row info_table'>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>BAR</div>
            <div className='tabellenzelle_r'>456</div>
          </div>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>TEST2</div>
            <div className='tabellenzelle_r'>DEF</div>
          </div>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>FOO</div>
            <div className='tabellenzelle_r'>123</div>
          </div>
          <div className='col-xs-3 info_table'>
            <div className='tabellenzelle_l'>TEST1</div>
            <div className='tabellenzelle_r'>ABC</div>
          </div>
        </div>
      </div>
    )
  }
}
