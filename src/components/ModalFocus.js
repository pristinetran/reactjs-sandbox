import React, {Component} from 'react';
import {Button, ButtonToolbar, FormControl, Modal} from 'react-bootstrap';

export default class ModalFocus extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleHide = this.handleHide.bind(this);

    this.state = {
      show: false
    };
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleHide() {
    this.setState({ show: false });
  }

  render() {
    return (
      <div>
        <ButtonToolbar>
          <Button bsStyle="primary" onClick={this.handleShow}>
            Launch demo modal
          </Button>
          <Modal
            show={this.state.show}
            onHide={this.handleHide}
            dialogClassName="custom-modal"
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-lg">
                Modal heading
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <h4>Wrapped Text</h4>
              <p>
                eum et nemo expedita. Consequuntur perspiciatis cumque dolorem.
              </p>
              <FormControl
                type='input'
                autoFocus
              />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.handleHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        </ButtonToolbar>
      </div>
    );
  }
}
