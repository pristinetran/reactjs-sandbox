import React, { Component } from 'react'
import helper from './tree-list-helper'
import options from './format-data.json'
import TreeList from '../modules/treeview/components/fixture/tree-list'

const { buildDataList, sanitizeDataForAjax, HashMapToArray } = helper

export default class Format extends Component {
  constructor (props, context) {
    super(props, context)

    this.state = {
      flatTree: {
        options: {},
        fieldType: 'format',
        keys: ['common.apps.handheld', 'common.mobile']
      },
      hierarchyTree: {
        options: {},
        fieldType: 'format',
        keys: ['common.apps.partners.wechat',
          'common.apps.partners.businesschat',
          'common.web.handheld']
      }
    }
  }

  componentWillMount () {
    let source = buildDataList(options)
    let data = [
      'common.mobile',
      'common.mobile.handheld',
      'common.mobile.tablet',
      'common.mobile.handheld.web',
      'common.mobile.handheld.app',
      'common.mobile.tablet.app',
      'common.apps.handheld',
      'common.apps.tablet',
      'common.apps.partners.wechat',
      'common.apps.partners.businesschat',
      'common.web.handheld'
    ]

    // eslint-disable-block no-param-reassign
    // console.warn('buildDataList', source)
    /*
      let expecting = [
         'common.mobile',
         'common.apps.handheld',
         'common.apps.tablet',
         'common.web.handheld',
         'common.apps.partners.wechat',
         'common.apps.partners.businesschat',
       ]
     */

    let dataObj = {
      'ww.emea.fi': 'Finland',
      'ww.emea.fr': 'France',
      'ww.emea.de': 'Germany'
    }

    console.warn('data source', HashMapToArray(dataObj))
    console.log('buildDataList Hierarchy', buildDataList(options, 'hierarchy'))

    console.warn('buildDataList Flat', buildDataList(options, 'flat'))
    console.warn('suck it out Flat', sanitizeDataForAjax(data, source, 'flat'))

    this.setState({
      flatTree: {
        ...this.state.flatTree,
        options: buildDataList(options, 'flat')
      },
      hierarchyTree: {
        ...this.state.hierarchyTree,
        options: buildDataList(options, 'hierarchy')
      }
    }, () => console.log('new State', this.state))
  }

  render () {
    // console.log('options', options, buildDataList(options, 'hierarchy'));
    // console.warn('suck it out Hierarchy', sanitizeDataForAjax(data, hierarchicalData, 'hierarchy'))

    return (
      <div>
        <h2>Render TreeView components</h2>
        <h3>FlatTree</h3>
        <TreeList treeClass={'ui-flat-tree'} displayTitle={false} {...this.state.flatTree} />
        <br />
        <h3>HierarchyTree</h3>
        <TreeList treeClass={'ui-hierarchy-tree'} displayTitle={false} {...this.state.hierarchyTree} />
      </div>
    )
  }
}
