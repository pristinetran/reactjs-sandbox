/* eslint no-console:0 */
/* eslint no-alert:0 */
import React from 'react'
import PropTypes from 'prop-types'
import Tree, { TreeNode } from 'rc-tree'
import '../../styles/rc-tree-index.css'
import '../../styles/rc-tree.css'
import classNames from 'classnames'

export default class TreeList extends React.Component {

  constructor (props) {
    super(props)
    const { options, fieldType, checkStrictly, keys } = props
    let treeType = fieldType === 'region' ? 'hierarchy' : 'flat'
    let data = options.arrayList || []

    if (data && data.length > 0 && fieldType === 'format') {
      data[0].title = `${data[0].title} Formats`
    } else if (fieldType === 'segment') {
      data[0].title = `${data[0].title} Segments`
    }

    this.state = {
      defaultExpandedKeys: keys,
      defaultSelectedKeys: keys,
      defaultCheckedKeys: keys,
      switchIt: true,
      data,
      checked: false,
      prevNodes: [],
      checkStrictly,
      treeType
    }
  }

  onExpand = (expandedKeys) => {
    console.log('onExpand', expandedKeys, arguments)
  };
  onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info)
    this.selKey = info.node.props.eventKey
  };
  onCheck = (checkedKeys, e) => {
    const last = (checkedKeys && checkedKeys.length > 0) ? [checkedKeys[checkedKeys.length - 1]] : ['null']

    this.setState({
      prevNodes: checkedKeys || []
    })

    console.warn('prevNodes', this.state.prevNodes, 'lastly-checked', last, 'checkedKeys', checkedKeys, 'event', e)
    // this.props.input.onChange(JSON.stringify(checkedKeys))
  }

  render () {
    const { displayTitle, treeClass } = this.props
    let { data, checkStrictly, defaultExpandedKeys, defaultSelectedKeys, defaultCheckedKeys } = this.state
    const isFlatView = treeClass === 'ui-flat-tree'

    const loop = data => {
      return data.map((item) => {
        const customLabel = (
          <span className='cus-label' style={{ display: 'block' }}>
            <span>{ displayTitle ? item.key : item.title }</span>
            <span style={{ color: 'gray', fontSize: '11px', marginLeft: '12px', marginTop: '-6px', display: 'block' }}>
              <i>({ displayTitle ? item.title : item.key })</i>
            </span>
          </span>
        )

        if (item.children) {
          return (<TreeNode key={item.key} title={customLabel}>
            {loop(item.children)}
          </TreeNode>)
        }
        return <TreeNode key={item.key} title={customLabel} />
      })
    }

    let treeNodes
    if (this.treeNodes && this.notReRender) {
      treeNodes = this.treeNodes
    } else {
      treeNodes = loop(data)
      this.treeNodes = treeNodes
    }

    const treeClassBuilder = classNames({
      'ui-flat-tree': isFlatView
      // 'rc-show-checkbox': this.state.checked
    })

    return (
      <Tree
        className={treeClassBuilder}
        defaultExpandAll
        showLine
        showIcon={false}
        checkable
        selectable={false}
        checkStrictly={checkStrictly}
        defaultExpandedKeys={defaultExpandedKeys}
        onExpand={this.onExpand}
        defaultCheckedKeys={defaultCheckedKeys}
        onSelect={this.onSelect} onCheck={this.onCheck}
      >
        {treeNodes}
      </Tree>
    )
  }
}

TreeList.propTypes = {
  keys: PropTypes.array,
  options: PropTypes.object,
  input: PropTypes.object,
  checkStrictly: PropTypes.bool,
  displayTitle: PropTypes.bool,
  fieldType: PropTypes.string,
  treeClass: PropTypes.string,
  onChange: () => {}
}

TreeList.defaultProps = {
  checkStrictly: false,
  displayTitle: true,
  fieldType: '',
  treeClass: 'ui-hierarchy-tree'
}
